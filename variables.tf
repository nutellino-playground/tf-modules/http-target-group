variable "health_check_timeout" {
  type    = "string"
  default = ""
}

variable "health_check_path" {
  type    = "string"
  default = ""
}

variable "health_check_protocol" {
  type    = "string"
  default = ""
}

variable "health_check_healthy_threshold" {
  type    = "string"
  default = "3"
}

variable "health_check_unhealthy_threshold" {
  type    = "string"
  default = "3"
}

variable "health_check_interval" {
  type    = "string"
  default = ""
}

variable "health_check_matcher" {
  type    = "string"
  default = ""
}

variable "name" {
  type = "string"
}

variable "port" {
  type = "string"
}

variable "vpc_id" {
  type = "string"
}

variable "alb_listener_arn" {
  type = "string"
}

variable "alb_host" {
  type = "string"
}

variable "alb_path" {
  type = "string"
}

variable "autoscaling_group_name" {
  type = "string"
}

variable "protocol" {
  type = "string"
}
