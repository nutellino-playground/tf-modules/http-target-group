# Http Target group

Create target group from ELB to autoscalinggroup

## Usage

```hcl

module "eks-lempdemo" {

  source = "git::https://gitlab.com/nutellino-playground/tf-modules/http-target-group.git?ref=0.0.2"
  name = "eks-lempdemo" 
  alb_listener_arn = "${aws_alb_listener.alb_application_listener.arn}"
  alb_host = "lempdemo.example.com"
  alb_path = "/*"
  autoscaling_group_name = "${module.eks_worker.autoscaling_group_name}"
  protocol = "HTTP"
  port = "8080"
  vpc_id = "x23123"
  health_check_protocol = "HTTP"
  health_check_matcher  = "200"
  health_check_healthy_threshold = "2"
  health_check_unhealthy_threshold = "2"
  health_check_path     = "/"
  health_check_timeout  = "5"
  health_check_interval = "10"

}
```