resource "null_resource" "alb_exists" {
  triggers {
    alb_name = "${var.alb_listener_arn}"
  }
}

resource "aws_lb_listener_rule" "alb_listener_rule" {
  listener_arn = "${var.alb_listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.target_group.arn}"
  }

  condition {
    field = "host-header"

    values = [
      "${var.alb_host}",
    ]
  }

  condition {
    field = "path-pattern"

    values = [
      "${var.alb_path}",
    ]
  }

  depends_on = [
    "null_resource.alb_exists",
  ]
}

resource "aws_lb_target_group" "target_group" {
  name     = "${var.name}-target-g"
  port     = "${var.port}"
  protocol = "${var.protocol}"
  vpc_id   = "${var.vpc_id}"

  stickiness {
    enabled = "false"
    type    = "lb_cookie"
  }

  deregistration_delay = "30"

  health_check {
    timeout             = "${var.health_check_timeout}"
    path                = "${var.health_check_path}"
    protocol            = "${var.health_check_protocol}"
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    interval            = "${var.health_check_interval}"
    matcher             = "${var.health_check_matcher}"
  }

  depends_on = [
    "null_resource.alb_exists",
  ]
}

resource "aws_autoscaling_attachment" "autoscaling_attachment" {
  autoscaling_group_name = "${var.autoscaling_group_name}"
  alb_target_group_arn   = "${aws_lb_target_group.target_group.arn}"
}
